﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test1
{
    public partial class Form1 : Form
    {
        

        public Form1()
        {
            InitializeComponent();
            StartListening(); // listen to the power change event without a button
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StartListening();
        }

        private void StartListening() 
        {
            SystemEvents.PowerModeChanged += new PowerModeChangedEventHandler(FontHandler); // register to an event and call the handler function
            statusLbl.Text = "מאזין";
            //this.eventHandlersCreated = true;
        }

        private void StopListening()
        {
            SystemEvents.PowerModeChanged -= new PowerModeChangedEventHandler(FontHandler);
            //this.eventHandlersCreated = false;
        }

        private void FontHandler(object sender, EventArgs e)
        {
            /*PowerStatus ps = SystemInformation.PowerStatus; get the power status with system information
            PowerLineStatus p = ps.PowerLineStatus; get the power line status offline or online 
            if (p == PowerLineStatus.Offline) 
            {
                statusLbl.Text = "מנותק";
            }
            else
            {
                statusLbl.Text = "מחובר";
            }*/

            statusLbl.Text = (SystemInformation.PowerStatus.PowerLineStatus == PowerLineStatus.Offline) ? "מנותק" : "מחובר"; // one liner
            //txtStatus.Text += string.Format("Installed fonts changed. {0}", Environment.NewLine);
        }
    }
}
